module Connection
  autoload :Builder, File.join(__dir__, "connection", "builder")
  autoload :DSL,     File.join(__dir__, "connection", "dsl")

  def connect(&block)
    builder = Builder.new
    proxy = DSL.proxy(block.binding.eval("self"), builder).new
    proxy.instance_exec(&block)
    builder.connection
  end
end
