module Connection
  module DSL
    module_function def proxy(context, builder)
      Class.new do
        define_method(:initialize) do
          context.instance_variables.each do |name|
            instance_variable_set(name, context.instance_variable_get(name))
          end
        end

        define_method(:request) do |&block|
          instance_exec(&block)
        end

        define_method(:with) do |&block|
          instance_exec(&block)
        end

        define_method(:method_missing) do |name, *args, &block|
          if builder.respond_to?(name)
            builder.__send__(name, *args, &block)
          else
            context.__send__(name, *args, &block)
          end
        end

        define_method(:respond_to_missing?) do |name|
          builder.respond_to?(name)
        end
      end
    end
  end
end
