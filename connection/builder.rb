module Connection
  class Builder
    def initialize
      @options = { dynamic: [] }
    end

    def proxy(url:)
      @options[:proxy_url] = url
    end

    def with(&block)
      @options[:dynamic] << block
    end

    def basic_auth(user:, password:)
      @options[:user] = user
      @options[:password] = password
    end

    def connection
      @options
    end
  end
end
