require_relative './connection'

class Client
  include Connection

  def initialize(user:, password:)
    @user = user
    @password = password
  end

  def connection
    @connection ||= connect do
      request do
        basic_auth user: @user, password: @password
        with { proxy url: dynamic_proxy_url }
      end
    end
  end

  def dynamic_proxy_url
    'URL'
  end
end
